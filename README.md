# Blender BatchRender and Layout
## Description

 This script will run Blender in batch mode and 
 render the specified .blend file with the same 
 settings. Then the output images will be merged
 into a grid layout in a bigger final image.

 The parameters are the following:

 Layout Options: "Basic", "Strip", "Default", "StripXL"
 Resolution: None or [width,height]
 Resolution scale: By default 100, means use the Resolution at 100%.
 Max render time: Maximum time in seconds for each frame.
 Scene: Path to the Blender scene.
 Composition path: Path to save the collage. None: will not do the collage, Default: follows pipeline spec. Custom: your provided path.


 None without "" usually means that you keep the scene values.

## Result
- [ ] [renders](https://gitlab.com/LMorante/blenderbarchrender/-/tree/main/renders)
- [ ] [final output](https://gitlab.com/LMorante/blenderbarchrender/-/blob/main/output/assetName.png)