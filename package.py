name = "Turntable"

version = "1.0.2"

authors = [
    "lmorante"
]

description = \
    """
    Asset turntable package.
    """

tools = [
    "turntabletool"
]

requires = [
    "python",
    # "ffmpeg_python",
    # "glob2",
    # "QtAwesome",
    # "QtPy",
]

uuid = "repository.%s" % name

build_command = 'python {root}/build.py {install}'

def commands():
    env.PYTHONPATH.append("{root}/src")
    env.PATH.append("{root}/bin")
