#   BLENDER BATCH RENDER
#
# This script will run Blender in batch mode and 
# render the specified .blend file with the same 
# settings. Then the output images will be merged
# into a grid layout in a bigger final image.
#
# -------------------------------------------------------------------------------------------------
# TODO Add overwrite functionality. Get existing renders and remove them form the frame list
# TODO Run on farm.
# TODO Instead of ffmpeg use openImageIO
# -------------------------------------------------------------------------------------------------
import os
import shutil
import glob
import subprocess
from blenderbatchrender.batchRender_UI import *

ROOT = os.path.dirname(__file__).replace("\\", "/")
PRE_RENDER_SCRIPT = "{0}/preRenderScript.py".format(ROOT) 
BLENDER_ROOT = "C:/Program Files/Blender Foundation/Blender 3.5"

class blender_batchRender():
    def __init__(self):
        pass

    def make_dir(self, path):
        """
        input a path to check if it exists, if not, it creates all the path
        :return: path string
        """
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def copyAndRenameFile(self, file, newName=""):
        original = file
        target = newName

        outDir = target.replace("\\", "/").rsplit("/", 1)[0]
        blender_batchRender.make_dir(self, outDir)
        shutil.copyfile(original, target)
        return target

    def convertView2Cmd(self, input):
        cmd = ""
        for e in range(len(input)):
            if e != 0:
                cmd += ","
            cmd += str(input[e])

        return cmd

    def layout_tempFiles(self, layout, renderFolder, renderFilename, tempFolder):
        orderList = layout['views']
        asset = renderFilename.rsplit("_", 1)[0]
        padding = len(renderFilename.rsplit("_", 1)[1].split(".")[0])
        extension = renderFilename.rsplit("_", 1)[1].split(".")[-1]
        temp = []
        foundFiles = glob.glob('{0}/{1}_*.{2}'.format(renderFolder, asset, extension))
        if len(foundFiles) > 0:
            for e in range(len(orderList)):
                for o_File in foundFiles:
                    o_Name = o_File.replace("\\", "/").rsplit("/", 1)[1].rsplit(".", 1)[0]
                    o_ID = o_Name.rsplit("_", 1)[1]
                    if orderList[e] == int(o_ID):
                        n_ID = str(e).zfill(padding)
                        t_Name = o_Name.rsplit("_", 1)
                        n_Name = t_Name[0] + "_" + t_Name[-1].replace(o_ID, n_ID)
                        n_File = "{0}/{1}.png".format(tempFolder, n_Name).replace("\\", "/")
                        print(n_File)
                        out = blender_batchRender.copyAndRenameFile(self, o_File, n_File)
                        temp.append(out)
        else:
            print("No renders found.")
        return temp

    def run_blenderBatchRender(self, layout, renderFolder, renderFilename, scenePath,
                               resolutionWidth, resolutionHeight, resolutionScale, maxRenderTime):

        render_output = "{0}/{1}".format(renderFolder, renderFilename)
        ui_settings = "{0} {1} {2} {3}".format(resolutionScale, maxRenderTime, resolutionWidth, resolutionHeight)
        frames = blender_batchRender.convertView2Cmd(self, layout['views'])
        blender_batchRender.make_dir(self, renderFolder)

        cmd = "{0}/blender.exe -b {1} --python {3} -o {2} -f {4} -- {5}".format(BLENDER_ROOT, scenePath, render_output, PRE_RENDER_SCRIPT, frames, ui_settings)
        print(cmd)
        subprocess.call(cmd, shell=False)
        return render_output

    def build_layerContactSheet(self, layout, outputFilename, outputFolder, tempFolder):
        asset = outputFilename.rsplit("_", 1)[0]
        padding = len(outputFilename.rsplit("_", 1)[1].split(".")[0])
        extension = outputFilename.rsplit("_", 1)[1].split(".")[-1]
        tile = layout['tile']
        templateName = layout['name']

        input = r"{0}/{1}_%0{3}d.{2}".format(tempFolder, asset, extension, padding)
        output = r"{0}/{1}_{3}.{2}".format(outputFolder, asset, extension, templateName)

        blender_batchRender.make_dir(self, outputFolder)
        cmd = r'ffmpeg -y -loglevel quiet -i {0} -filter_complex "tile={2}:margin=0" {1} '.format(input, output, tile)
        cmd = r'ffmpeg -y -i {0} -filter_complex "tile={2}:margin=0" {1} '.format(input, output, tile)
        subprocess.call(cmd, shell=False)
        return output

    def open_collageFile(self,COMPOSITION_PATH):
        path = COMPOSITION_PATH
        path = os.path.realpath(path)
        os.startfile(path)