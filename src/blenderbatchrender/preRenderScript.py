# This script will be executed inside the blender scene, before rendering.
#-------------------------------------------------------------------------------------------------
import sys
import bpy

argv = sys.argv
for e in range(len(argv)):
    if "--" == argv[e]:
        print(e)
        ui_scale = int(argv[e + 1])
        ui_renderTime = int(argv[e + 2])
        ui_width = int(argv[e + 3])
        ui_height = int(argv[e + 4])

print(ui_scale)
print(ui_renderTime)
print(ui_width)
print(ui_height)

if ui_width != 0:
    bpy.context.scene.render.resolution_x = ui_width   #resolutionWidth[0]

if ui_height != 0:
    bpy.context.scene.render.resolution_y = ui_height  #resolutionHeight[1]

# Override Resolution Scale
if ui_scale != 0:
    bpy.context.scene.render.resolution_percentage = ui_scale

# Override Max RenderTime
if ui_renderTime != 0:
    bpy.context.scene.cycles.time_limit = ui_renderTime
    

# General Overrides
bpy.context.scene.render.use_border = False
bpy.context.scene.render.image_settings.color_mode = 'RGBA'
bpy.context.scene.render.image_settings.color_depth = '8'


