import os
import sys
import shutil
import subprocess
import qtawesome
from typing import Optional
from qtpy import QtCore, QtGui, QtWidgets
from blenderbatchrender import layout_templates as lt

# -------------------------------------------------------------------------------------------------
# TODO Add overwrite checkbox
# TODO fix thumbnail re-scaling
# TODO fix install - dont generate .egg-info
# -------------------------------------------------------------------------------------------------

WINDOW_NAME = 'Batch render tool'
THUMBNAIL_PATH = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '../..', 'thumbnails'))
EXAMPLES_PATH = os.path.join(os.environ['USERPROFILE'], 'Documents')
HELP_DOCS = ''


def launchDoc():
    subprocess.Popen(['xdg-open', HELP_DOCS])

class AttributeNumericWdg(QtWidgets.QWidget):
    def __init__(self, name, min=None, max=None, default=None):
        QtWidgets.QWidget.__init__(self)
        self.label = name
        self.min = min
        self.max = max
        self.default = default

        self.initUi()

    def initUi(self):
        # Main layout
        self.mainLay = QtWidgets.QHBoxLayout()
        self.mainLay.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.mainLay)

        # Attribute Name
        self.label = QtWidgets.QLabel(self.label, parent=self)
        self.mainLay.addWidget(self.label)

        # Attribute value
        self.spinBoxWdg = QtWidgets.QSpinBox(parent=self)
        self.spinBoxWdg.setToolTip('Use scene value if set to 0')
        if self.min:
            self.spinBoxWdg.setMinimum(self.min)
        if self.max:
            self.spinBoxWdg.setMaximum(self.max)
        if self.default:
            self.spinBoxWdg.setValue(self.default)
        self.mainLay.addWidget(self.spinBoxWdg)

    def value(self):
        return self.spinBoxWdg.value()

class FolderPickerWdg(QtWidgets.QWidget):
    PICKER_TEXT = '...'

    def __init__(self, name, custom=True, fileMode:bool = False):
        QtWidgets.QWidget.__init__(self)
        self.name = name
        self._custom = custom
        self._fileMode = fileMode

        self.initUi()
        self.setupUi()
        self.initConnectSignals()

    def initUi(self):
        # Main layout
        self.mainLay = QtWidgets.QVBoxLayout()
        self.mainLay.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.mainLay)

        # Header layout
        self.headerWdg = QtWidgets.QWidget(parent=self)
        self.headerLayout = QtWidgets.QHBoxLayout(self.headerWdg)
        self.headerLayout.setContentsMargins(0, 0, 0, 0)
        self.headerLayout.setAlignment(QtCore.Qt.AlignLeft)
        self.mainLay.addWidget(self.headerWdg)

        # Folder Name
        if self._custom:
            self.labelCheckBox = QtWidgets.QCheckBox(parent=self.headerWdg)
            self.headerLayout.addWidget(self.labelCheckBox)

        self.nameLabel = QtWidgets.QLabel(self.name, parent=self.headerWdg)
        self.headerLayout.addWidget(self.nameLabel)

        # Path picker widget
        self.pathPickerWdg = QtWidgets.QWidget(parent=self)
        self.pathPickerLayout = QtWidgets.QHBoxLayout(self.pathPickerWdg)
        self.pathPickerLayout.setContentsMargins(0, 0, 0, 0)

        self.mainLay.addWidget(self.pathPickerWdg)

        # Path display
        self.pathLineEdit = QtWidgets.QLineEdit(parent=self.pathPickerWdg)
        self.pathPickerLayout.addWidget(self.pathLineEdit)

        # Path picker
        self.pickerButton = QtWidgets.QPushButton(parent=self.pathPickerWdg)
        self.pickerButton.setIcon(qtawesome.icon("mdi6.folder-search", color='white'))
        self.pathPickerLayout.addWidget(self.pickerButton)

    def setupUi(self):
        self.setEnabled(not self._custom)

    def initConnectSignals(self):
        if self.isCustom():
            self.labelCheckBox.clicked.connect(self.setEnabled)
        self.pickerButton.clicked.connect(self.openfile if self._fileMode else self.openfolder)

    def openfile(self):
        openPath = QtWidgets.QFileDialog.getOpenFileName(self, dir=EXAMPLES_PATH, filter="(*.blend)")
        self.pathLineEdit.setText(openPath[0])

    def openfolder(self):
        openPath = QtWidgets.QFileDialog.getExistingDirectory(self)
        self.pathLineEdit.setText(openPath)

    def isCustom(self) -> bool:
        return self._custom
    
    def setEnabled(self, value:bool = True) -> None:
        self.pathLineEdit.setEnabled(value)
        self.pickerButton.setEnabled(value)

    def getPath(self) -> Optional[str]:
        if self.isCustom():
            if not self.labelCheckBox.isChecked():
                return None
        path = self.pathLineEdit.text()
        return path if path else None


class batchRender_UI(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.initUi()
        self.initConnectSignals()

    def initUi(self):
        # MAIN WINDOW
        self.setWindowTitle(WINDOW_NAME)
        self.setWindowIcon(QtGui.QIcon(THUMBNAIL_PATH + r"/illu.ico"))

        # MAIN MENU
        menuBar = QtWidgets.QMenuBar()
        self.layout().setMenuBar(menuBar)
        helpMenu = menuBar.addMenu('&Help')
        helpMenu.addAction(
            'See Documentation',
            launchDoc)

        # CENTRAL WIDGET
        self.centralWdg = QtWidgets.QWidget(parent=self)
        self.centralWdg.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
        self.setCentralWidget(self.centralWdg)

        # MAIN LAYOUT
        self.mainLay = QtWidgets.QVBoxLayout(self.centralWdg)
        self.mainLay.setAlignment(QtCore.Qt.AlignTop)
        self.mainLay.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        
        # Top Bar
        self.topBarWdg = QtWidgets.QWidget(parent=self.centralWdg)
        self.topBarLayout = QtWidgets.QVBoxLayout(self.topBarWdg)
        self.topBarLayout.setContentsMargins(0, 0, 0, 0)
        self.mainLay.addWidget(self.topBarWdg)

        # Config folders
        self.scenePathWdg = FolderPickerWdg(name='Blender scene path', custom=False, fileMode=True)
        self.renderPathWdg = FolderPickerWdg(name='Custom render path', custom=True)
        self.outputPathWdg = FolderPickerWdg(name='Custom output path', custom=True)
        
        self.topBarLayout.addWidget(self.scenePathWdg)
        self.topBarLayout.addWidget(self.renderPathWdg)
        self.topBarLayout.addWidget(self.outputPathWdg)

        # Template picker
        self.templateWdg = QtWidgets.QWidget(parent=self)
        self.templateLayout = QtWidgets.QHBoxLayout(self.templateWdg)
        self.templateLayout.setContentsMargins(0, 0, 0, 0)
        self.templateLayout.setAlignment(QtCore.Qt.AlignCenter)
        self.mainLay.addWidget(self.templateWdg)

        self.templateLabel = QtWidgets.QLabel(self)
        self.templateLabel.setText("Layout :")
        self.templateLayout.addWidget(self.templateLabel)

        self.templateComboBox = QtWidgets.QComboBox(self)
        # foundLayoutTemplates = lt.getLayoutTemplates()
        foundLayoutTemplates = lt.LAYOUTS.keys()
        for foundLayoutTemplate in foundLayoutTemplates:
            self.templateComboBox.addItem(foundLayoutTemplate)
        self.templateLayout.addWidget(self.templateComboBox)
        self.templateComboBox.currentTextChanged.connect(self.imagesThumbnails)
        
        self.templateImgLabel = QtWidgets.QLabel(self)
        self.templateImgLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.imageBasic = QtGui.QPixmap(THUMBNAIL_PATH + r"/basic.png")
        self.templateImgLabel.setPixmap(self.imageBasic)
        self.mainLay.addWidget(self.templateImgLabel)

        # Render settings
        self.renderSettingsWdg = QtWidgets.QWidget(parent=self)
        self.renderSettingsLayout = QtWidgets.QVBoxLayout(self.renderSettingsWdg)
        self.renderSettingsLayout.setContentsMargins(0, 0, 0, 0)
        self.renderSettingsLayout.setAlignment(QtCore.Qt.AlignHCenter)
        self.mainLay.addWidget(self.renderSettingsWdg)

        self.maxRenderTimeWdg = AttributeNumericWdg(name='Max render time', min=0, default=0)
        self.maxRenderTimeWdg.label.setToolTip('Represented in seconds.')
        self.renderSettingsLayout.addWidget(self.maxRenderTimeWdg)

        self.resScaleWdg = AttributeNumericWdg(name='Resolution Scale', min=0, max=100, default=100)
        self.renderSettingsLayout.addWidget(self.resScaleWdg)

        self.resWidthWdg = AttributeNumericWdg(name='Resolution Width', min=0, max=5000)
        self.renderSettingsLayout.addWidget(self.resWidthWdg)

        self.resHeightWdg = AttributeNumericWdg(name='Resolution Height', min=0, max=5000)
        self.renderSettingsLayout.addWidget(self.resHeightWdg)

        # Apply Button
        self.renderButton = QtWidgets.QPushButton(parent=self.centralWdg, text="Render")
        self.mainLay.addWidget(self.renderButton)

        # self.setLayout(self.mainLay)

    def initConnectSignals(self):
        self.renderButton.clicked.connect(self.build)

    def imagesThumbnails(self, value):
        """Update thumbnail image to match selected layout template"""
        image = QtGui.QPixmap("{0}/{1}.png".format(THUMBNAIL_PATH, value.lower()))
        self.templateImgLabel.setPixmap(image)

    def ui_getScenePath(self):
        return self.scenePathWdg.getPath()

    def ui_getRenderPath(self):
        defaultPath = self.ui_getScenePath().split(".")[0]
        customPath = self.renderPathWdg.getPath()
        return customPath if customPath else defaultPath
    
    def ui_getOutputPath(self):
        defaultPath = self.ui_getRenderPath() + r"/output"
        customPath = self.outputPathWdg.getPath()
        return customPath if customPath else defaultPath

    def ui_getlayoutTemplate(self):
        layoutName = self.templateComboBox.currentText()
        return lt.LAYOUTS[layoutName]

    def getLayoutTemplates():
        pass

    def build(self):
        # Gather UI data
        scenePath = self.ui_getScenePath()
        renderPath = self.ui_getRenderPath()
        outputPath = self.ui_getOutputPath()
        layout = self.ui_getlayoutTemplate()
        resolutionScale = self.resScaleWdg.value()
        resolutionWidth = self.resWidthWdg.value()
        resolutionHeight = self.resHeightWdg.value()
        maxRenderTime = self.maxRenderTimeWdg.value()

        # Generate paths
        sceneName = scenePath.rsplit("/", 1)[-1].split(".")[0]
        renderName = "{0}_###.png".format(sceneName)
        tmpPath = "{0}/temp".format(renderPath)

        from blenderbatchrender.blender_batchRender import blender_batchRender
    
        # Render Blender File.
        run = blender_batchRender.run_blenderBatchRender(self, 
                                                         layout=layout, 
                                                         renderFolder=renderPath, 
                                                         renderFilename=renderName, 
                                                         scenePath=scenePath,
                                                         resolutionWidth=resolutionWidth, 
                                                         resolutionHeight=resolutionHeight, 
                                                         resolutionScale=resolutionScale, 
                                                         maxRenderTime=maxRenderTime)

        # Make temp files for collage
        temp = blender_batchRender.layout_tempFiles(self, 
                                                    layout=layout, 
                                                    renderFolder=renderPath, 
                                                    renderFilename=renderName, 
                                                    tempFolder=tmpPath)

        # Make collage
        output = blender_batchRender.build_layerContactSheet(self,
                                                             layout=layout, 
                                                             outputFilename=renderName, 
                                                             outputFolder=outputPath, 
                                                             tempFolder=tmpPath)
        # CleanUp
        # time.sleep(5)
        shutil.rmtree(tmpPath)

        # open collage when composite finish
        # Report
        print("Done! \n%s" % output)
        open = blender_batchRender.open_collageFile(self, outputPath)


def launch(args):
    app = QtWidgets.QApplication(args)
    win = batchRender_UI()
    win.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    launch(sys.argv)
