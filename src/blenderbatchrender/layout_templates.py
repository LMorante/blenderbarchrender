# TODO: invert templates to make the default rotation clockwise. Now the templates are based on the camera.

# Sequence of Renders from 0° to 360 0° in 9 Frames, 8 Frames usable.
#
#   F       R+      R       R-      B       L-      L       L+   | Point of view (Front, Right, Left, Back)
#   0°      45°     90°    135°    180°    225°    270°    310°  | if rotating camera
#   0°     310°    270°    225°    180°    135°     90°     45°  | if rotating asset
#   0       1       2       3       4       5       6       7    | frames
#
# note: turntable animation slould be configured by rotating the camera or the asset. If not templates will not match. 
#

LAYOUTS = {
    'Basic':{
        'name': "Basic",
        'tile': "3x1",
        'views': [7,0,1],
    },
    'Strip':{
        'name': "Strip",
        'tile': "5x1",
        'views': [6,7,0,1,2],
    },
    'Default':{
        'name': "Default",
        'tile': "3x2",
        'views': [7,0,1,6,4,2],
    },
    'StripXL':{
        'name': "StripXL",
        'tile': "8x1",
        'views': [3,4,5,6,7,0,1,2],
    },
}
