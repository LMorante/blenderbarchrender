#!/usr/bin/env python

import os
import os.path
import shutil
import sys
import stat

# TODO: RN a {root}/build dir is generated when building with rez-build --install
#   How is this dir intended to be used?
#   Should the {root}/build dir be generated in another path?
#   What is the build.rxt file for?
#   Do I need the dir or the build.rxt file? What uses them?
#   Can I delete the dir after the data is copied to the install dir?
# note: {root}/build was added to gitignore

def build(source_path, build_path, install_path, targets):
    """
    Simple build function that copies the relevant files 
    from sources to install path.
    """

    def _build():
        # python source
        src_py = os.path.join(source_path, "src")
        dest_py = os.path.join(build_path, "src")

        if not os.path.exists(dest_py):
            shutil.copytree(src_py, dest_py)

        # binaries
        mode = (stat.S_IRUSR | stat.S_IRGRP |
                stat.S_IXUSR | stat.S_IXGRP)

        src_bin = os.path.join(source_path, "bin")
        dest_bin = os.path.join(build_path, "bin")

        if not os.path.exists(dest_bin):
            shutil.copytree(src_bin, dest_bin)

            for name in os.listdir(dest_bin):
                filepath = os.path.join(dest_bin, name)
                os.chmod(filepath, mode)

    def _install():
        for name in ("bin", "src", "thumbnails"):
            src = os.path.join(source_path, name)
            dest = os.path.join(install_path, name)
            
            if os.path.exists(dest):
                shutil.rmtree(dest)

            print(src)
            print(dest)
            shutil.copytree(src, dest)

    # _build()

    if "install" in (targets or []):
        _install()


if __name__ == '__main__':
    build(
        source_path=os.environ['REZ_BUILD_SOURCE_PATH'],
        build_path=os.environ['REZ_BUILD_PATH'],
        install_path=os.environ['REZ_BUILD_INSTALL_PATH'],
        targets=sys.argv[1:]
    )
